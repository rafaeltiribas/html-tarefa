# html-tarefa

Atividade em grupo do PS da IN Júnior.

Se juntem em duplas ou trios para fazerem uma página do grupo de vocês
Página principal
Cabeçalho com o título da página com o nome do grupo.
Uma descrição conjunta do grupo (não tenham medo de brincar e ser criativos).
Menu de navegação com links para página individual de cada um.
Rodapé contendo:
● Um slogan para o grupo
● um iframe com o mapa do google apontando para o prédio do IC na UFF
(Desafio)

## Página individual:
Cabeçalho com o título da página.
Uma foto qualquer.
Um parágrafo de apresentação falando um pouco sobre você. (mínimo de 50 palavras)
Lista não ordenada com 5 hobbies.
Lista ordenada com seus top 5 qualquer coisa (filmes, músicas, games, memes, etc..).
Rodapé contendo links para:
● sua página do Gitlab
● suas redes sociais (Pode redirecionar para a home, se assim preferir)
● página de contato.
Um link para página de contato do membro em questão
Um link para voltar para página inicial

## Página de contato individual:

Cabeçalho com o título da página.
Formulário contendo os seguintes campos: Nome, e-mail, telefone e campo para
mensagem.
Botão para enviar a mensagem para seu endereço de e-mail.
(OBS:Não precisa ser funcional)
Adicionar um áudio e um vídeo (Desafio).
Link para voltar à página do membro.
Rodapé igual ao da página do membro.

## Regras:
1. Usem apenas HTML.
2. A página principal e de contato devem somar no mínimo 20 tags diferentes.
3. Faça bom uso de tags semânticas e comentários.
PS.: Não se atenha ao básico. Seja criativo! Pesquise como adicionar mais itens à
página (músicas, vídeos, tabelas, etc..).
Para adicionar áudio em sua página utilize a biblioteca de áudio do youtube lá você
pode baixar áudios sem custo.